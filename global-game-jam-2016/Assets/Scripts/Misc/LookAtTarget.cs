﻿using UnityEngine;
using System.Collections;

public class LookAtTarget : MonoBehaviour
{
    [SerializeField]
    private Transform _Target;
    public Transform Target
    {
        get { return _Target; }
        set { _Target = value; }
    }

	void LateUpdate ()
    {
	    if(Target)
        {
            transform.LookAt(Target);
        }
	}
}
