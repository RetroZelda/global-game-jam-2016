﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StorybookEventsNS;

namespace InputEvents
{
    public class InputEvent : IPage
    {
        public bool IsJoystick { get; private set; }
        public object BindKey { get; private set; }

        public InputEvent(bool joyStick, object bindKey) 
        {
            IsJoystick = joyStick;
            BindKey = bindKey;
        }
	}

    public class KeyInputEvent : InputEvent
    {
        public bool KeyHitState { get; private set; }
        public KeyInputEvent(bool keyHitState, bool joyStick, object bindKey) : base(joyStick, bindKey)
        {
            KeyHitState = keyHitState;
        }
    }

    public class AxisInputEvent : InputEvent
    {
        public Dictionary<string, float> AxisMap { get; private set; }
        public AxisInputEvent(Dictionary<string, float> axisMap, bool joyStick, object bindKey) : base(joyStick, bindKey)
        {
            AxisMap = axisMap;
        }
    }
}