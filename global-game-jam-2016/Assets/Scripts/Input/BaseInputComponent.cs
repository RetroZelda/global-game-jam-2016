﻿using UnityEngine;
using System.Collections;
using StorybookEventsNS;
using System;
using InputEvents;

/// <summary>
/// A place where the default keybind exist
/// TODO: Make the keybinds a saved asset thing
/// </summary>
public abstract class BaseInputComponent : MonoBehaviour, IReader
{
    public delegate void KeyAction(bool bHitState);
    public delegate void AxisAction(Vector2 values);

    private JoystickAssigner assigner;
    private InputBehavior input;

    private JoystickBinding _FocusedJoystick = null;
    public JoystickBinding FocusedJoystick
    {
        get
        {
            return _FocusedJoystick;
        }
        // NOTE: This will NOT return the joystick to the assigner
        set
        {
            _FocusedJoystick = value;
            if(_FocusedJoystick != null)
            {
                JoystickID = JoystickAssigner.GetJoyID(_FocusedJoystick);
            }
            else
            {
                JoystickID = "";
            }
        }
    }
    public string JoystickID { get; private set; }

    [SerializeField]
    // We wont want to automatically get a joystick if the joystick is to be attached dynamically(e.g. vehicle, god, etc)
    private bool AutomaticallyRequestJoystick = true;

    private bool _bRegistered = false;

    public KeyAction OnCircle;
    public KeyAction OnCross;
    public KeyAction OnSquare;
    public KeyAction OnTriangle;
    public AxisAction OnLeftAnalog;

    public virtual void ReadPage(IPage page)
    {
        if(page is KeyInputEvent)
        {
            KeyInputEvent keyEvent = page as KeyInputEvent;
            if(keyEvent.BindKey.ToString() == JoystickID + "triangle")
                OnTriangle.Invoke(keyEvent.KeyHitState);
            if (keyEvent.BindKey.ToString() == JoystickID + "square")
                OnSquare.Invoke(keyEvent.KeyHitState);
            if (keyEvent.BindKey.ToString() == JoystickID + "circle")
                OnCircle.Invoke(keyEvent.KeyHitState);
            if (keyEvent.BindKey.ToString() == JoystickID + "cross")
                OnCross.Invoke(keyEvent.KeyHitState);
        }
        else if(page is AxisInputEvent)
        {
            AxisInputEvent AxisEvent = page as AxisInputEvent;
            if (AxisEvent.BindKey.ToString() == JoystickID + "movement")
            { 
                OnLeftAnalog.Invoke(new Vector2(AxisEvent.AxisMap[JoystickID + "Axis0"], AxisEvent.AxisMap[JoystickID + "Axis1"]));
            }
        }
    }
    
    protected virtual void Awake()
    {
        input = FindObjectOfType<InputBehavior>();
        assigner = FindObjectOfType<JoystickAssigner>();

        if (AutomaticallyRequestJoystick)
        {
            FocusedJoystick = assigner.RequestOpenJoystick();
            JoystickID = JoystickAssigner.GetJoyID(FocusedJoystick);
        }
    }

    protected virtual void OnEnable()
    {
        RegisterInput();
    }

    protected virtual void OnDisable()
    {
        UnregisterInput();
    }

    // DS4 controller cheat sheet
    // Left Analog:
    //              X - axis 0
    //              Y - axis 1
    // Right Analog:
    //              X - axis 3
    //              Y - axis 6
    // L1 - btn 5
    // R1 - btn 6
    // L2 - axis 4; btn 7
    // R2 - axis 5; btn 8
    // L3 - 
    // R3 - 
    // Triangle - btn 4 
    // Square - btn 1
    // Circle - btn 3
    // Cross - btn 2
    // DPAD Left - axis 7 (-1)
    // DPAD Right - axis 7 (1)
    // DPAD Up - axis 8 (1)
    // DPAD Down - axis 8 (-1)
    public virtual void RegisterInput()
    {
        if (FocusedJoystick == null || _bRegistered == true)
            return;

        input.RegisterAxis(JoystickID + "movement", false, JoystickID + "Axis0", JoystickID + "Axis1");
        input.RegisterKey(JoystickID + "triangle", JoystickAssigner.JoystickKeycode(JoystickID, 4), BroadcastKeyState.PRESSED);
        input.RegisterKey(JoystickID + "square", JoystickAssigner.JoystickKeycode(JoystickID, 1), BroadcastKeyState.PRESSED);
        input.RegisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.PRESSED);
        input.RegisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.PRESSED);
        
        // listen for the input events
        StoryBook.Chapter("Input_" + JoystickID + "triangle").Subscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "square").Subscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "circle").Subscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "cross").Subscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "movement").Subscribe<AxisInputEvent>(this);

        _bRegistered = true;
    }

    public virtual void UnregisterInput()
    {
        if (FocusedJoystick == null || _bRegistered == false)
            return;

        input.UnregisterAxis(JoystickID + "movement", false, JoystickID + "Axis0", JoystickID + "Axis1");
        input.UnregisterKey(JoystickID + "triangle", JoystickAssigner.JoystickKeycode(JoystickID, 4), BroadcastKeyState.PRESSED);
        input.UnregisterKey(JoystickID + "square", JoystickAssigner.JoystickKeycode(JoystickID, 1), BroadcastKeyState.PRESSED);
        input.UnregisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.PRESSED);
        input.UnregisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.PRESSED);

        // listen for the input events
        StoryBook.Chapter("Input_" + JoystickID + "triangle").Unsubscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "square").Unsubscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "circle").Unsubscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "cross").Unsubscribe<KeyInputEvent>(this);
        StoryBook.Chapter("Input_" + JoystickID + "movement").Unsubscribe<AxisInputEvent>(this);

        _bRegistered = false;
    }
    
	protected virtual void Update ()
    {
        /*
        // NOTE: polling axis info
        for (int i = 0; i < 10; ++i ) // HACK: only axis 9
        {
            float fVal = Input.GetAxis(JoystickID + "Axis" + i);
            if ((fVal > 0.0f || fVal < 0.0f) && fVal > -1.0f && fVal < 1.0f)
                Debug.Log(JoystickID + "Axis" + i + ": " + fVal);
        }
        
        // NOTE: polling button
        for(int i = 1; i < 20; ++i)
        {
            if(Input.GetKeyDown(JoystickKeycode(JoystickID, i)))
            {
                Debug.Log(JoystickID + "Btn" + i);
            }
        }
        */
    }

}
