﻿using UnityEngine;
using System.Collections;
using System;

public class HumanInput : BaseInputComponent
{
    private CPlayer _player;
    public CPlayer Player { get { return _player; } }

	private Vector3 _CurDir;



    protected override void Awake()
    {
        base.Awake();

        _player = gameObject.GetComponent<CPlayer>();

        OnCircle += HandleCircle;
        OnCross += HandleCross;
        OnSquare += HandleSquare;
        OnTriangle += HandleTriangle;
        OnLeftAnalog += HandleLeftAnalog;

    }
    public void HandleCircle(bool bHitState)
    {

    }

    public void HandleCross(bool bHitState)
    {

    }

    public void HandleSquare(bool bHitState)
    {
		
		_player.Dash(_CurDir);
    }

    public void HandleTriangle(bool bHitState)
    {

    }

    public void HandleLeftAnalog(Vector2 values)
    {
        // Debug.Log(JoystickID + " : " + values);

		_CurDir = Vector3.Normalize(new Vector3(-values.y, 0.0f, -values.x));

		_player.Move(_CurDir);
    }
}
