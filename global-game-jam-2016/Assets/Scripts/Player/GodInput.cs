﻿using UnityEngine;
using System.Collections;

public class GodInput : BaseInputComponent
{
    private CGod _god;
    private CPlayer _player;
    private RockSpawner _RockSpawner;


    private GodManager _GodManager;

    protected override void Awake()
    {
        base.Awake();

        _god = gameObject.GetComponent<CGod>();
        _player = gameObject.GetComponent<CPlayer>();
        _RockSpawner = FindObjectOfType<RockSpawner>();

        _GodManager = FindObjectOfType<GodManager>();

        OnCircle += HandleCircle;
        OnCross += HandleCross;
        OnSquare += HandleSquare;
        OnTriangle += HandleTriangle;
        OnLeftAnalog += HandleLeftAnalog;
    }
    public void HandleCircle(bool bHitState)
    {
		_god.PushRightWall();
    }

    public void HandleCross(bool bHitState)
    {
        _RockSpawner.CreateRockAtMarker();
    }

    public void HandleSquare(bool bHitState)
    {
		_god.PushLeftWall();
    }

    public void HandleTriangle(bool bHitState)
    {
        _RockSpawner.CreateRockRandom();
    }

    public void HandleLeftAnalog(Vector2 values)
    {
        // _RockSpawner.MoveMarker(new Vector3(values.y, values.x, 0.0f ));
        _RockSpawner.MoveMarker(new Vector3(0.0f, values.x, 0.0f ));
    }
}
