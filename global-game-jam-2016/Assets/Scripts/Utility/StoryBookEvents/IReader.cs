namespace StorybookEventsNS
{
    // Interface class for an object to read a page
    public interface IReader
    {
        void ReadPage(IPage page);
    }
}