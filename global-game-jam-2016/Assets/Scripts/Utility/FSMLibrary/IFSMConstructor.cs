namespace FSMLibraryNS
{
    public interface IFSMConstructor
    {
        string Chapter {get;}
        int ChapterHash {get;}

        void Build();
        void Destroy();
    }
}