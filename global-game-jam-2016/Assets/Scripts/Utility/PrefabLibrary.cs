﻿using UnityEngine;
using System.Collections;

public class PrefabLibrary : MonoBehaviour
{
    [SerializeField]
    public GameObject HumanPrefab;

    [SerializeField]
    public GameObject GodPrefab;

    [SerializeField]
    public GameObject GodManagerPrefab;
    
    [SerializeField]
    public GameObject InputControllerPrefab;

    [SerializeField]
    public GameObject CanvasePrefab;

    [SerializeField]
    public GameObject FSMPrefab;
}
