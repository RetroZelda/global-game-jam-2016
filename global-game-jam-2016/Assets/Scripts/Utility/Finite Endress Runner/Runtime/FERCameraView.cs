﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class FERCameraView : MonoBehaviour {

    private Camera _Camera;
    private Plane[] _Frustum;

    void Awake ()
    {
        _Camera = GetComponent<Camera>();
        _Frustum = GeometryUtility.CalculateFrustumPlanes(_Camera);
	}
	
    public bool IsObjectOnScreen(Collider col)
    {
        return GeometryUtility.TestPlanesAABB(_Frustum, col.bounds);
    }
}

