﻿using UnityEngine;
using System.Collections;

namespace FER
{
    [System.Serializable]
    public class FERTileDescriptor
    {
        // NOTE: Wall spacing should be a scalar value (0 -> 1)
        [SerializeField]
        public float WallSpace_Start;

        [SerializeField]
        public float WallSpace_End;

        [SerializeField]
        public float Wallheight;

        [SerializeField]
        public Vector3 GroundScale = Vector3.one;

        [SerializeField]
        public Vector3 WallScale = Vector3.one;

        public bool IsTapered { get { return WallSpace_Start == WallSpace_End; } }

        public float WallSpace
        {
            set
            {
                WallSpace_Start = value;
                WallSpace_End = value;
            }
        }
    }

    [System.Serializable]
    public struct FERTileInfo
    {
        public Mesh FloorMesh;
        public Mesh WallMesh;

        public Material[] FloorMaterials;
        public Material[] WallMaterials;
    }
}