﻿using UnityEngine;
using System.Collections.Generic;

namespace FER
{
    /// <summary>
    /// THe main controller for the Finite Endress Runner(FER) System
    /// </summary>
    public class FERController : MonoBehaviour
    {
        [SerializeField]
        public float _WallHeight;

        [SerializeField]
        public Vector3 _GroundScale = Vector3.one;

        [SerializeField]
        public Vector3 _WallScale = Vector3.one;

        [SerializeField]
        private Mesh _GroundMesh;

        [SerializeField]
        private Mesh _WallMesh;

        [SerializeField]
        private Material[] _GroundMaterials;

        [SerializeField]
        private Material[] _WallMaterials;

        [SerializeField]
        private int _ActiveTileCount;

        [SerializeField]
        private float _RunnerSpeed;

        [SerializeField]
        private float _RunnerTime;

        private Queue<FERTile> _Tiles;
        
        void Awake()
        {
            _Tiles = new Queue<FERTile>();
        }

        void Start()
        {
            // prebuild the starting tiles
            FERTileInfo info = new FERTileInfo();
            info.FloorMesh = _GroundMesh;
            info.WallMesh = _WallMesh;
            info.FloorMaterials = _GroundMaterials;
            info.WallMaterials = _WallMaterials;

            Vector3 v3CurTilePosition = transform.position;
            FERTileDescriptor prevDescriptor = null;
            for(int nTileCount = 0; nTileCount < _ActiveTileCount; ++nTileCount)
            {
                GameObject newTile = new GameObject("Tile", typeof(FERTile));
                Transform tileTrans = newTile.GetComponent<Transform>();
                FERTile tile = newTile.GetComponent<FERTile>();

                tileTrans.parent = transform;
                tileTrans.localPosition = v3CurTilePosition;
                tileTrans.localRotation = Quaternion.identity;

                prevDescriptor = GenerateNewDescriptor(prevDescriptor);
                tile.BuildTile(prevDescriptor, info);

                v3CurTilePosition += new Vector3(0.0f, 0.0f, tile._TileSize.z);
                _Tiles.Enqueue(tile);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private FERTileDescriptor GenerateNewDescriptor(FERTileDescriptor prevDescriptor)
        {
            // TODO: if we have a previous descriptor, we need to ensure they will connect(new start = prev end)
            FERTileDescriptor newDescriptor = new FERTileDescriptor();
            newDescriptor.Wallheight = _WallHeight;
            newDescriptor.WallSpace = 1.0f - (0.1f * _Tiles.Count);
            newDescriptor.GroundScale = _GroundScale;
            newDescriptor.WallScale = _WallScale;

            return newDescriptor;
        }
    }
}