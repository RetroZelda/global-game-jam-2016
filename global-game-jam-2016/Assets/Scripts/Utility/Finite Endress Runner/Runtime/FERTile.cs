﻿using UnityEngine;
using System.Collections;

namespace FER
{
    public class FERTile : MonoBehaviour
    {
        /// <summary>
        /// Size of the Tile.  X,Z defines the "walkable" area, and Y defines the wall height
        /// </summary>
        public Vector3 _TileSize;

        private FERTileDescriptor _TileDescriptor;
        private FERTileInfo _TileInfo;
                
        public virtual void BuildTile(FERTileDescriptor tileDesc, FERTileInfo tileInfo)
        {
            _TileDescriptor = tileDesc;
            _TileInfo = tileInfo;
            Rebuild();
        }

        public virtual void Rebuild()
        {
            // wipe out all children
            while(transform.childCount > 0)
            {
                GameObject.Destroy(transform.GetChild(transform.childCount - 1).gameObject);
            }
            
            float v3FinalWallHeight =  (_TileDescriptor.WallScale * _TileDescriptor.Wallheight).y;
            _TileSize = Vector3.Scale(_TileInfo.FloorMesh.bounds.size, _TileDescriptor.GroundScale);
            _TileSize.y = v3FinalWallHeight;

            // build the ground
            GameObject ground = new GameObject("ground", typeof(BoxCollider), typeof(MeshRenderer), typeof(MeshFilter));
            Transform groundTransform = ground.GetComponent<Transform>();
            BoxCollider groundCollider = ground.GetComponent<BoxCollider>();
            MeshRenderer groundRenderer = ground.GetComponent<MeshRenderer>();
            MeshFilter groundMeshFilter = ground.GetComponent<MeshFilter>();

            groundTransform.parent = transform;
            groundTransform.localPosition = Vector3.zero;
            groundTransform.localRotation = Quaternion.identity;
            groundTransform.localScale = _TileDescriptor.GroundScale;

            groundCollider.center = _TileInfo.FloorMesh.bounds.center;
            groundCollider.size = _TileInfo.FloorMesh.bounds.size;

            groundMeshFilter.mesh = _TileInfo.FloorMesh;

            groundRenderer.materials = _TileInfo.FloorMaterials;

            // build each wall
            float fWallSide = 1.0f;
            for(int nWallIndex = 0; nWallIndex < 2; ++nWallIndex)
            { 
                GameObject wall = new GameObject("wall" + nWallIndex, typeof(BoxCollider), typeof(MeshRenderer), typeof(MeshFilter));
                Transform wallTransform = wall.GetComponent<Transform>();
                BoxCollider wallCollider = wall.GetComponent<BoxCollider>();
                MeshRenderer wallRenderer = wall.GetComponent<MeshRenderer>();
                MeshFilter wallMeshFilter = wall.GetComponent<MeshFilter>();

                // TODO: Clean this shit up
                wallTransform.parent = transform;
                float fWallMidpoint = Vector3.Scale(_TileDescriptor.WallScale, Vector3.Scale(_TileDescriptor.GroundScale, _TileInfo.FloorMesh.bounds.size)).x * (((_TileDescriptor.WallSpace_Start + _TileDescriptor.WallSpace_End) / 2.0f) * fWallSide);
                wallTransform.localPosition = new Vector3(fWallMidpoint, _TileInfo.WallMesh.bounds.size.y * 0.5f, 0.0f);
                wallTransform.localRotation = Quaternion.identity; // TODO: Use the start and end points to determine the needed rotation
                wallTransform.localScale = Vector3.Scale(_TileDescriptor.GroundScale, _TileDescriptor.WallScale); // _TileDescriptor.WallScale;
                wallTransform.localScale = new Vector3(_TileInfo.WallMesh.bounds.size.x, wallTransform.localScale.y, wallTransform.localScale.z);

                wallCollider.center = new Vector3(_TileInfo.WallMesh.bounds.center.x, (v3FinalWallHeight * 0.5f) - wallTransform.localPosition.y, _TileInfo.WallMesh.bounds.center.z);
                wallCollider.size = new Vector3(_TileInfo.WallMesh.bounds.size.x, v3FinalWallHeight, _TileInfo.WallMesh.bounds.size.z);

                wallMeshFilter.mesh = _TileInfo.WallMesh;

                wallRenderer.materials = _TileInfo.WallMaterials;

                fWallSide *= -1;
            }
        }
    }
}