﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using StorybookEventsNS;
using DG.Tweening;
using System;

[RequireComponent(typeof(CanvasGroup))]
public class BlackFader : MonoBehaviour
{
    // these are for events
    public static string _Fade = "BlackFader";
    public static int FadeHash = _Fade.GetHashCode();

    private CanvasGroup Group;

    private static Queue<FadeEvent> FadeQueue = new Queue<FadeEvent>();
    private static bool InCoroutene = false;
    private static BlackFader Instance;
    void Awake()
    {
        Instance = this;
        Group = GetComponent<CanvasGroup>();

        StoryBook.Chapter(FadeHash).Subscribe<FadeEvent>(Fade);
    }

    void OnDestroy()
    {
        StoryBook.Chapter(FadeHash).Unsubscribe<FadeEvent>(Fade);
    }

    private void Fade(IPage page)
    {
        FadeEvent fade = page as FadeEvent;
        float fFadeTo = 0.0f;

        if (fade.FadeIn)
        {
            fFadeTo = 1.0f;
        }

        Group.DOFade(fFadeTo, fade.Time);
    }

    public static void Fade(bool bFadeIn, float fTime = 1.0f, Action onFadeComplete = null)
    {
        FadeQueue.Enqueue(new FadeEvent(bFadeIn, fTime, onFadeComplete));

        if (!InCoroutene)
        {
            if (Instance)
            {
                Instance.StartCoroutine(FadeQueueHandler());
            }
        }
    }

    public static WaitForSeconds WaitForFade(bool bFadeIn, float fTime = 1.0f)
    {
        Fade(bFadeIn, fTime);
        return new WaitForSeconds(fTime);
    }

    private static IEnumerator FadeQueueHandler()
    {
        Instance.Group.blocksRaycasts = true;
        InCoroutene = true;
        while (FadeQueue.Count > 0)
        {
            FadeEvent e = FadeQueue.Dequeue();
            StoryBook.Chapter(FadeHash).Set(e);

            yield return new WaitForSeconds(e.Time);

            if (e.OnFadeComplete != null)
            {
                e.OnFadeComplete.Invoke();
            }
        }
        InCoroutene = false;
        Instance.Group.blocksRaycasts = false;
    }
}

public class FadeEvent : IPage
{
    public float Time { get; private set; }
    public bool FadeIn { get; private set; }
    public Action OnFadeComplete { get; private set; }
    public FadeEvent(bool fadein, float time, Action onFadeComplete)
    {
        Time = time;
        FadeIn = fadein;
        OnFadeComplete = onFadeComplete;
    }
}
