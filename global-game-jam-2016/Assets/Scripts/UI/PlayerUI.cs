﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerUI : MonoBehaviour {

    private RectTransform _UI = null;
    private RectTransform _UIDashCooldown = null;
    private LookAtTarget _UIDashCooldownLookAt = null;
    private Image _UIDashCooldownFG = null;
    private Image _UIDashCooldownBG = null;

    public RectTransform UI
    {
        get { return _UI; }
        private set { _UI = value; }
    }

    private Color _PlayerColor = Color.white;
    public Color PlayerColor
    {
        get { return _PlayerColor; }
        set
        {
            _PlayerColor = value;

            if(_UIDashCooldownFG != null)
                _UIDashCooldownFG.color = _PlayerColor;
            if (_UIDashCooldownBG != null)
                _UIDashCooldownBG.color = _PlayerColor;
        }
    }

    void Awake ()
    {
        _UI = transform.FindChild("UI").transform as RectTransform;

        _UIDashCooldown = _UI.FindChild("DashCooldown_Circle") as RectTransform;
        _UIDashCooldownFG = _UIDashCooldown.FindChild("fg").GetComponent<Image>();
        _UIDashCooldownBG = _UIDashCooldown.FindChild("bg").GetComponent<Image>();
        _UIDashCooldownLookAt = _UIDashCooldown.GetComponent<LookAtTarget>();

    }

    void Start()
    {
        _UIDashCooldownLookAt.Target = Camera.main.transform;
        _UIDashCooldownFG.color = _PlayerColor;
        _UIDashCooldownBG.color = _PlayerColor;
    }

    public void SetDashCooldownLamba(float fLamda)
    {
        _UIDashCooldownFG.fillAmount = fLamda;
    }
	
}
