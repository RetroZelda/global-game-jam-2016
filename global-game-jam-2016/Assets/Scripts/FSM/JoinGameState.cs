﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FSMLibraryNS;
using DG.Tweening;
using StorybookEventsNS;
using InputEvents;
using System;

public class JoinGameState : BaseState, IReader
{
    private class PlayerJoinData
    {
        public Transform JoinUI { get; set; }
        public Text PlayerText { get; private set; }
        public Text StatusText { get; private set; }
        public int PlayerNumber { get; set; }
        public Color PlayerColor { get; set; }
        public string JoystickID { get; set; }
        public Vector3 SpawnPosition { get; set; }

        private GameObject _PlayerObject;
        public GameObject PlayerObject
        {
            get
            {
                if(_PlayerObject == null)
                {
                    PrefabLibrary _PrefabLibrary = GameObject.FindObjectOfType<PrefabLibrary>();
                    JoystickAssigner _Assigner = GameObject.FindObjectOfType<JoystickAssigner>();

                    _PlayerObject = GameObject.Instantiate(_PrefabLibrary.HumanPrefab, SpawnPosition, Quaternion.identity) as GameObject;
                    HumanInput myInput = _PlayerObject.GetComponent<HumanInput>();
                    CPlayer player = _PlayerObject.GetComponent<CPlayer>();

                    // ensure we get a color
                    player.SetColor(PlayerColor);

                    /*
                    // get hte proper controller
                    myInput.FocusedJoystick = _Assigner.RequestOpenJoystick(JoystickID);
                    myInput.RegisterInput();
                    */
                }

                return _PlayerObject;
            }
        }

        private bool _IsJoined;
        public bool IsJoined
        {
            get { return _IsJoined; }
            set
            {
                _IsJoined = value;
                if(JoinUI != null)
                {
                    if(_IsJoined)
                    {
                        StatusText.text = ": JOINED";
                        PlayerObject.SetActive(true);
                    }
                    else
                    {
                        StatusText.text = ": Press [X]";
                        PlayerObject.SetActive(false);
                    }
                }
            }
        }

        public PlayerJoinData(Transform joinUI, Vector3 spawnPosition, int num, Color col, string joyID)
        {
            JoinUI = joinUI;
            PlayerText = JoinUI.transform.FindChild("Text/txt_player").GetComponent<Text>();
            StatusText = JoinUI.transform.FindChild("Text/txt_status").GetComponent<Text>();

            SpawnPosition = spawnPosition;
            PlayerNumber = num;
            PlayerColor = col;
            IsJoined = false;
            JoystickID = joyID;
            
            PlayerText.text = "P" + PlayerNumber;
            PlayerText.color = PlayerColor;
            PlayerObject.SetActive(false);
        }
    };

    private Transform _JoinMenu;
    private Transform _JoinContainer;
    private Transform _CountdownPanel;
    private Transform _InstructionsPanel;
    private Transform _GodSelectionPanel;

    private Transform _JoinTemplate;

    private Text _CountdownText;
    private Text _InstructionsText;

    private bool _bReadyForCountdown = false;
    private bool _bCountdownStarted = false;
    int nTotalNumberOfPlayers = 0;
    int nRequiredNumberOfPlayersToStart = 2;
    private PlayerJoinData[] _PotentialPlayers;
    
    private CSpawnPositions _SpawnPointGenerator;
    private Vector3[] _v3SpawnPoints = null;
    private MaterialHolder m_MatHold;

    public void ReadPage(IPage page)
    {
        if(page is KeyInputEvent)
        {
            KeyInputEvent e = page as KeyInputEvent;
            foreach(PlayerJoinData player in _PotentialPlayers)
            {
                if(e.BindKey.ToString().Contains(player.JoystickID))
                {
                    if (e.BindKey.ToString().Contains("cross") && !player.IsJoined)
                    {
                        player.IsJoined = true;
                        nTotalNumberOfPlayers++;
                    }
                    else if (e.BindKey.ToString().Contains("circle") && !_bCountdownStarted && player.IsJoined)
                    {
                        player.IsJoined = false;
                        nTotalNumberOfPlayers--;
                    }                        
                }
            }
            CheckForCountdownStart();
        }
    }

    public override void OnCreate()
    {
        base.OnCreate();

        _JoinMenu = Canvas.FindChild("JoinMenu");
        _JoinContainer = _JoinMenu.FindChild("JoinContainer");
        _CountdownPanel = _JoinMenu.FindChild("CountdownPanel");
        _InstructionsPanel = _JoinMenu.FindChild("InstructionsPanel");
        _GodSelectionPanel = _JoinMenu.FindChild("GodSelectionPanel");

        _JoinTemplate = _JoinContainer.FindChild("JoinTemplate");

        _CountdownText = _CountdownPanel.FindChild("text_Countdown").GetComponent<Text>();
        _InstructionsText = _InstructionsPanel.FindChild("text_Title").GetComponent<Text>();

        _SpawnPointGenerator = GameObject.FindObjectOfType<CSpawnPositions>();
        m_MatHold = GameObject.FindObjectOfType<MaterialHolder>();
    }

    public override void OnEnter()
    {
        base.OnEnter();

        BlackFader.Fade(false, 1.0f);

        _JoinMenu.gameObject.SetActive(true);
        _InstructionsPanel.gameObject.SetActive(true);
        _JoinContainer.gameObject.SetActive(true);
        _CountdownPanel.gameObject.SetActive(false);
        _GodSelectionPanel.gameObject.SetActive(false);

        // register the X buttons and create all the player join ui
        _JoinTemplate.gameObject.SetActive(false);
        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        JoystickAssigner assigner = GameObject.FindObjectOfType<JoystickAssigner>();
        int nPlayerCount = 1;
        _PotentialPlayers = new PlayerJoinData[assigner.Joysticks.Count];
        _v3SpawnPoints = _SpawnPointGenerator.SpawnPositions(assigner.Joysticks.Count); // get our potential spawn points

        foreach (JoystickBinding joyBind in assigner.Joysticks)
        {
            string JoystickID = JoystickAssigner.GetJoyID(joyBind);
            input.RegisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.RELEASED);
            input.RegisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + JoystickID + "cross").Subscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "circle").Subscribe<KeyInputEvent>(this);

            // handle the join UI
            GameObject newJoin = GameObject.Instantiate(_JoinTemplate.gameObject) as GameObject;
            newJoin.transform.SetParent(_JoinContainer);
            newJoin.SetActive(true);
            _PotentialPlayers[nPlayerCount - 1] = new PlayerJoinData(newJoin.transform, _v3SpawnPoints[nPlayerCount - 1], nPlayerCount++, m_MatHold.GetColor(m_MatHold.NextColorIndex()), JoystickID);
        }

        // make the text type in
        _bReadyForCountdown = false;
        _bCountdownStarted = false;
        string szCache = _InstructionsText.text;
        _InstructionsText.text = "";
        _InstructionsText.DOText(szCache, 0.0f).OnComplete(InstructionsFinishedDisplay);
    }

    private void InstructionsFinishedDisplay()
    {
        _bReadyForCountdown = true;
    }

    private void CheckForCountdownStart()
    {
        if(!_bCountdownStarted && _bReadyForCountdown && nTotalNumberOfPlayers >= nRequiredNumberOfPlayersToStart)
        {
            _bCountdownStarted = true;
            Fader.StartCoroutine(DoCountdown(5)); // HACK: Using Fader because i needed a monobehaviour
        }
    }

    private void StartGodDecision()
    {
        Fader.StartCoroutine(DoGodDecision());
    }

    public override void OnExit()
    {
        base.OnExit();

        foreach(Transform joiner in _JoinContainer)
        {
            if(joiner != _JoinTemplate)
            {
                GameObject.Destroy(joiner.gameObject);
            }
        }
        _JoinMenu.gameObject.SetActive(false);
        _PotentialPlayers = null;
        nTotalNumberOfPlayers = 0;

        // register the X buttons and create all the player join ui
        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        JoystickAssigner assigner = GameObject.FindObjectOfType<JoystickAssigner>();
        foreach (JoystickBinding joyBind in assigner.Joysticks)
        {
            string JoystickID = JoystickAssigner.GetJoyID(joyBind);
            input.UnregisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.RELEASED);
            input.UnregisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + JoystickID + "cross").Unsubscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "circle").Unsubscribe<KeyInputEvent>(this);
        }
    }

    private IEnumerator DoGodDecision()
    {
        UnityEngine.Random.seed = (int)Time.realtimeSinceStartup;
        _GodSelectionPanel.gameObject.SetActive(true);
        int nOrderCount = UnityEngine.Random.Range(3 * nTotalNumberOfPlayers, 6 * nTotalNumberOfPlayers) ;
        int nCurCount = 0;
        int nActivePlayerIndex = 0;
        while(nOrderCount > nCurCount)
        {
            // disable the arrow
            _PotentialPlayers[nActivePlayerIndex].JoinUI.FindChild("Arrow").gameObject.SetActive(false);

            // get hte player that we will be focused on
            bool bStop = false;
            do
            {
               
                nActivePlayerIndex++;
                if (nActivePlayerIndex >= _PotentialPlayers.Length)
                    nActivePlayerIndex = 0;
                bStop = _PotentialPlayers[nActivePlayerIndex].IsJoined;
            } while (!bStop);

            // get hte arrow for this player
            _PotentialPlayers[nActivePlayerIndex].JoinUI.FindChild("Arrow").gameObject.SetActive(true);
            yield return new WaitForSeconds(0.05f * nCurCount++);
        }

        // we have hte winner, flash it
        _PotentialPlayers[nActivePlayerIndex].JoinUI.FindChild("Arrow").DOShakeScale(2.0f);
        yield return new WaitForSeconds(4.0f);

        // we are finished, compile the list of the players to be used
        string[] playerJoystickIDs = new string[nTotalNumberOfPlayers];
        HumanInput[] _Humans = new HumanInput[nTotalNumberOfPlayers];
        int nCurIndex = 0;
        foreach (PlayerJoinData player in _PotentialPlayers)
        {
            if (player.IsJoined)
            {
                playerJoystickIDs[nCurIndex] = player.JoystickID;
                _Humans[nCurIndex++] = player.PlayerObject.GetComponent<HumanInput>();
            }
        }
        FSMLibrary.Chapter(FSMConstructor._ChapterHash).ChangeState(typeof(GameState), _PotentialPlayers[nActivePlayerIndex].JoystickID, (object)(playerJoystickIDs), (object)(_Humans));
    }

    private IEnumerator DoCountdown(float fSeconds)
    {
        _InstructionsPanel.gameObject.SetActive(false);
        _CountdownPanel.gameObject.SetActive(true);

        float fTimeLeft = fSeconds;
        _CountdownText.text = fTimeLeft.ToString("00");
        // bool bFirst = true;
        while (fTimeLeft > 0.0f)
        {
            string szPrevText = _CountdownText.text;
            _CountdownText.text = fTimeLeft.ToString("00");
            /*
            if(szPrevText != _CountdownText.text || bFirst)
            {
                bFirst = false;
                _CountdownText.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                _CountdownText.transform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.75f);
                _CountdownText.CrossFadeAlpha(1.0f, 0.0f, true);
                _CountdownText.CrossFadeAlpha(0.0f, 0.75f, true);
            }
            */
            fTimeLeft -= Time.deltaTime * 0.8f;
            yield return null;
        }
        _CountdownPanel.gameObject.SetActive(false);
        StartGodDecision();
    }
}
