﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FSMLibraryNS;
using DG.Tweening;
using StorybookEventsNS;
using InputEvents;

public class MainMenuState : BaseState, IReader
{
    private RectTransform _MainMenu;
    private Text _PressToPlayText;
    private bool _bFade = false;
    private bool _bFadeDir = false;
    private bool _bExiting = false;

    private GameObject _MainMenuWorld;

    public override void OnCreate()
    {
        base.OnCreate();
        _MainMenu = Canvas.FindChild("MainMenu") as RectTransform;
        _PressToPlayText = _MainMenu.FindChild("text_PressToPlay").GetComponent<Text>();
        _MainMenuWorld = GameObject.Find("MainMenuWorld");
    }

    public override void OnEnter()
    {
        base.OnEnter();

        BlackFader.Fade(false, 1.0f);
        _MainMenu.gameObject.SetActive(true);
        _bFade = true;
        _bExiting = false;
        FadeText();

        // listen for all conmnected controller buttons
        InputBehavior input = Object.FindObjectOfType<InputBehavior>();
        JoystickAssigner assigner = Object.FindObjectOfType<JoystickAssigner>();
        foreach(JoystickBinding joyBind in assigner.Joysticks)
        {
            string JoystickID = JoystickAssigner.GetJoyID(joyBind);
            input.RegisterKey(JoystickID + "triangle", JoystickAssigner.JoystickKeycode(JoystickID, 4), BroadcastKeyState.RELEASED);
            input.RegisterKey(JoystickID + "square", JoystickAssigner.JoystickKeycode(JoystickID, 1), BroadcastKeyState.RELEASED);
            input.RegisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.RELEASED);
            input.RegisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.RELEASED);

            StoryBook.Chapter("Input_" + JoystickID + "triangle").Subscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "square").Subscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "circle").Subscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "cross").Subscribe<KeyInputEvent>(this);
        }

		GameObject.FindObjectOfType<CAudioMan>().PlayMainMenu();
        _MainMenuWorld.SetActive(true);
    }

    public virtual void ReadPage(IPage page)
    {
        if (_bExiting)
            return;
        if(page is KeyInputEvent)
        {
            _bExiting = true;
            KeyInputEvent debugOnlyReally = page as KeyInputEvent;
            Debug.Log("Main Menu Input by: " + debugOnlyReally.BindKey);
            BlackFader.Fade(true, 1.0f, ExitState);
        }
    }

    private void ExitState()
    {
        FSMLibrary.Chapter(FSMConstructor._ChapterHash).ChangeState(typeof(JoinGameState));
    }

    private void FadeText()
    {
        if (!_bFade)
            return;

        Color theColor = _PressToPlayText.color;

        if (_bFadeDir)
        {
            theColor.a = 1.0f;
        }
        else
        {
            theColor.a = 0.75f;
        }
        _PressToPlayText.DOColor(theColor, 0.15f).OnComplete(FadeText);
        _bFadeDir = !_bFadeDir;
    }

    public override void OnExit()
    {
        base.OnExit();
        _bFade = false;
        _MainMenu.gameObject.SetActive(false);
        _MainMenuWorld.SetActive(false);

        // stop listening to all conmnected controller buttons
        InputBehavior input = Object.FindObjectOfType<InputBehavior>();
        JoystickAssigner assigner = Object.FindObjectOfType<JoystickAssigner>();
        foreach (JoystickBinding joyBind in assigner.Joysticks)
        {
            string JoystickID = JoystickAssigner.GetJoyID(joyBind);
            input.UnregisterKey(JoystickID + "triangle", JoystickAssigner.JoystickKeycode(JoystickID, 4), BroadcastKeyState.RELEASED);
            input.UnregisterKey(JoystickID + "square", JoystickAssigner.JoystickKeycode(JoystickID, 1), BroadcastKeyState.RELEASED);
            input.UnregisterKey(JoystickID + "circle", JoystickAssigner.JoystickKeycode(JoystickID, 3), BroadcastKeyState.RELEASED);
            input.UnregisterKey(JoystickID + "cross", JoystickAssigner.JoystickKeycode(JoystickID, 2), BroadcastKeyState.RELEASED);

            StoryBook.Chapter("Input_" + JoystickID + "triangle").Unsubscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "square").Unsubscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "circle").Unsubscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + JoystickID + "cross").Unsubscribe<KeyInputEvent>(this);
        }
    }
}
