﻿using UnityEngine;
using System.Collections;
using FSMLibraryNS;
using System;
using StorybookEventsNS;
using InputEvents;

public class GameState : BaseState, IReader
{
    public static string GAME_EVENT_CHAPTER = "GameStateChapter";
    public static int GAME_EVENT_CHAPTER_HASH = GAME_EVENT_CHAPTER.GetHashCode();

    private PrefabLibrary _PrefabLibrary;
    private JoystickAssigner _Assigner;
    private CSpawnPositions _SpawnPointGenerator;
    private GodManager _GodManager;
    private CAudioMan _AudioManager;
    private CCameraMove _CameraMover;

    private string _DefaultGodJoystickId;
    private string[] _PlayerJoystickIds;
    private HumanInput[] _Humans;
    private GodInput _God;

	private CRandomZPlacement[] _Blocks;

    private bool _bGameRestarting;

    public override void OnCreate()
    {
        base.OnCreate();

        // get references to any scene game objects
        _PrefabLibrary = GameObject.FindObjectOfType<PrefabLibrary>() as PrefabLibrary;
        _Assigner = GameObject.FindObjectOfType<JoystickAssigner>();
        _SpawnPointGenerator = GameObject.FindObjectOfType<CSpawnPositions>();
        _GodManager = GameObject.FindObjectOfType<GodManager>();
        _AudioManager = GameObject.FindObjectOfType<CAudioMan>();
        _CameraMover = GameObject.FindObjectOfType<CCameraMove>();
    }
    public override void OnEnter()
    {
        base.OnEnter();

        _bGameRestarting = false;

        // spawn the god
        GameObject theGod = GameObject.Instantiate(_PrefabLibrary.GodPrefab) as GameObject;
        _God = theGod.GetComponent<GodInput>();

		_Blocks = GameObject.FindObjectsOfType<CRandomZPlacement>(); 

        // spawn the players
        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        int nIndex = 0;
        HumanInput defaultGod = null;
        foreach (string szJoyID in _PlayerJoystickIds)
        {
            HumanInput myInput = _Humans[nIndex++];     
            
            if(myInput.FocusedJoystick == null)
            {
                myInput.FocusedJoystick = _Assigner.RequestOpenJoystick(szJoyID);
                myInput.RegisterInput();
            }
                   
            if (szJoyID == _DefaultGodJoystickId)
                defaultGod = myInput;
        }
        
        // set the initial god
        _GodManager.ForceRefresh();
        _GodManager.SetGod(defaultGod);
        defaultGod.gameObject.SetActive(false);
        
        // run kyles shit
        _AudioManager.PlayGameState();
        _CameraMover.Reset();
        _CameraMover.Play();
        
        // sub to the events
        StoryBook.Chapter(GAME_EVENT_CHAPTER_HASH).Subscribe<SacrificeCollisionEvent>(this);
        StoryBook.Chapter(GAME_EVENT_CHAPTER_HASH).Subscribe<ScreenDeathCollisionEvent>(this);

    }

    public override void OnExit()
    {
        base.OnExit();

        // unsub to the events
        StoryBook.Chapter(GAME_EVENT_CHAPTER_HASH).Subscribe<SacrificeCollisionEvent>(this);
        StoryBook.Chapter(GAME_EVENT_CHAPTER_HASH).Subscribe<ScreenDeathCollisionEvent>(this);

        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();

        // despawn god
        _GodManager.StopGod();
        GameObject.Destroy(_God.gameObject);

        // despawn players
        foreach (HumanInput human in _Humans)
        {
            human.UnregisterInput();
            _Assigner.ReturnOpenJoystick(human.FocusedJoystick);
            human.FocusedJoystick = null;
            GameObject.Destroy(human.gameObject);
        }
        _Humans = null;

        // despawn rocks
        foreach (GameObject rock in GameObject.FindGameObjectsWithTag("Rock"))
        {
            GameObject.Destroy(rock);
        }
    }

    public override void OnPause()
    {
        base.OnPause();

        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        foreach (string szJoyID in _PlayerJoystickIds)
        {
            // register all for the "options" button
            input.UnregisterKey(szJoyID + "options", JoystickAssigner.JoystickKeycode(szJoyID, 10), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + szJoyID + "options").Unsubscribe<KeyInputEvent>(this);
        }

            SetInputAvailability(false);
        _CameraMover.Pause();
    }

    public override void OnResume()
    {
        base.OnResume();
        SetInputAvailability(true);
        _CameraMover.Play();

        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        foreach (string szJoyID in _PlayerJoystickIds)
        {
            // register all for the "options" button
            input.RegisterKey(szJoyID + "options", JoystickAssigner.JoystickKeycode(szJoyID, 10), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + szJoyID + "options").Subscribe<KeyInputEvent>(this);
        }
    }

    public override void RetrievePassthrough(params object[] _PassthroughObjects)
    {
        base.RetrievePassthrough(_PassthroughObjects);
        _DefaultGodJoystickId = _PassthroughObjects[0] as string;
        _PlayerJoystickIds = _PassthroughObjects[1] as string[];
        _Humans = _PassthroughObjects[2] as HumanInput[];
    }

    private void SetInputAvailability(bool bActive)
    {
        foreach (HumanInput human in _Humans)
        {
            human.enabled = bActive && (_God.FocusedJoystick.JoystickIndex != human.FocusedJoystick.JoystickIndex);
        }

        _God.enabled = bActive;
    }
    private void RestartGame(HumanInput winningPlayer)
    {
        // start the process with any old coroutene
        if (!_bGameRestarting)
            _PrefabLibrary.StartCoroutine(RestartGameRoutine(winningPlayer));
        else
            Debug.LogWarning("RestartGame() FAILED! Restart process already running!");
    }
    
    private IEnumerator RestartGameRoutine(HumanInput winningPlayer)
    {
        _bGameRestarting = true;

        // disable all input
        SetInputAvailability(false);

        // play any sacrificial animation
        if (winningPlayer != null)
        {
            // Disable player UI for cutscene
            winningPlayer.Player.UIEnable = false;
            yield return new WaitForSeconds(7.0f); // wait for cut scene
        }
        else
        {
            yield return new WaitForSeconds(1.0f); // hold for effect
        }

        // fade screen out
        BlackFader.Fade(true);
        yield return new WaitForSeconds(1.5f);

        // check if we have a winner
        if(winningPlayer != null)
        {
            // Reenable player UI after cutscene
            winningPlayer.Player.UIEnable = true;
            winningPlayer.Player.TurnOffCut();
            _GodManager.SetGod(winningPlayer);
        }

        // reset the players at the beginning who arent gods
        Vector3[] v3SpawnPoints = _SpawnPointGenerator.SpawnPositions(_PlayerJoystickIds.Length);

		// Random the block placements2
		RandomTheBlocks();

        int nIndex = 0;
        foreach (Vector3 v3Spawn in v3SpawnPoints)
        {
            _Humans[nIndex].GetComponent<Rigidbody>().velocity = Vector3.zero;
            _Humans[nIndex].transform.position = v3Spawn;
            _Humans[nIndex].gameObject.SetActive(_God.FocusedJoystick.JoystickIndex != _Humans[nIndex].FocusedJoystick.JoystickIndex);
            ++nIndex;
        }

        // remove all rocks
        foreach(GameObject rock in GameObject.FindGameObjectsWithTag("Rock"))
        {
            GameObject.Destroy(rock);
        }

        // reset camera
        _CameraMover.Reset();

        // fade screen in
        BlackFader.Fade(false);
        yield return new WaitForSeconds(1.5f);

        // start the camera
        _CameraMover.Play();

        // enable input
        SetInputAvailability(true);

        _bGameRestarting = false;
    }

    public void ReadPage(IPage page)
    {
        if(page is KeyInputEvent)
        {
            KeyInputEvent e = page as KeyInputEvent;
            if(e.BindKey.ToString().Contains("options"))
            {
                FSMLibrary.Chapter(FSMConstructor._ChapterHash).PushState(typeof(PauseState), _PlayerJoystickIds);
            }
        }
        else if(page is SacrificeCollisionEvent)
        {
            Debug.Log("Player won this round!");
            SacrificeCollisionEvent e = page as SacrificeCollisionEvent;

            e.Human.Player.IKilledMyself();
            RestartGame(e.Human);
        }
        else if(page is ScreenDeathCollisionEvent)
        {
            ScreenDeathCollisionEvent e = page as ScreenDeathCollisionEvent;

            Debug.LogWarning("Disabling object " + e.Human.name);
            //e.Human.gameObject.GetComponent<CPlayer>().PlayDeath();
            e.Human.gameObject.SetActive(false);
     

            // check if all our humans are disabled
            HumanInput activeGod = null;
            bool bPlayedDeath = false;
            foreach (HumanInput human in _Humans)
            {
                if (human.isActiveAndEnabled && !bPlayedDeath)
                {
                    bPlayedDeath = true;
                    human.GetComponent<CPlayer>().PlayDeath();
                }

                if (human.FocusedJoystick.JoystickIndex == _God.FocusedJoystick.JoystickIndex)
                {
                    activeGod = human;

                    if(!bPlayedDeath)
                    {
                        bPlayedDeath = true;
                        human.GetComponent<CPlayer>().PlayDeath();
                    }
                   
                }
                if (human.gameObject.activeSelf == true)
                {
                    return;
                }
            }

            // we are here so the god has won
            activeGod.GetComponent<CPlayer>().WonAsGod();
            RestartGame(null); // no winning player; same god
        }
    }

	private void RandomTheBlocks()
	{
		foreach(CRandomZPlacement RZP in _Blocks)
		{
			RZP.RANDOMDISSHIET();
		}

	}
}
