﻿using UnityEngine;
using System.Collections;
using FSMLibraryNS;

public class BaseState : IState
{
    protected RectTransform Canvas { get; private set; }
    protected BlackFader Fader { get; private set; }

    public virtual void OnCreate()
    {
        Canvas = GameObject.Find("Canvas").transform as RectTransform; 
        Fader = Canvas.transform.FindChild("BlackFader").GetComponent<BlackFader>();
    }

    public virtual void OnDestroy() { }
    public virtual void OnEnter() { }
    public virtual void OnExit() { }
    public virtual void OnPause() { }
    public virtual void OnResume() { }
    public virtual void PriorityUpdate() { }
    public virtual void Update() { }
    public virtual void LateUpdate() { }
    public virtual void RetrievePassthrough(params object[] _PassthroughObjects) { }

}
