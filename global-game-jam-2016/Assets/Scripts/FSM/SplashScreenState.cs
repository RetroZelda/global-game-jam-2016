﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FSMLibraryNS;
using DG.Tweening;

public class SplashScreenState : BaseState
{
    private Transform _SplashScreen;
    private Image[] _SplashImages;

    int nCurSplashImage = 0;

    public override void OnCreate()
    {
        base.OnCreate();

        _SplashScreen = Canvas.FindChild("SplashScreen");
        _SplashImages = _SplashScreen.GetComponentsInChildren<Image>();
    }

    public override void OnEnter()
    {
        _SplashScreen.gameObject.SetActive(true);
        BlackFader.Fade(true, 0.0f);
        nCurSplashImage = -1;

        // hacky way to start them all invisible
        foreach(Image splash in _SplashImages)
        {
            splash.DOFade(0.0f, 0.0f);
        }
        FadeNextImage();
    }

    public override void Update()
    {
        base.Update();

        if(Input.GetKeyDown(KeyCode.Space))
            FSMLibrary.Chapter(FSMConstructor._ChapterHash).ChangeState(typeof(MainMenuState));
    }

    public override void OnExit()
    {
        base.OnExit();

        _SplashScreen.gameObject.SetActive(false);
    }

    private void FadeNextImage()
    {
        nCurSplashImage++;
        if(nCurSplashImage < _SplashImages.Length)
        {
            _SplashImages[nCurSplashImage].StartCoroutine(HandleSplash(_SplashImages[nCurSplashImage]));
        }
        else
        {
            FSMLibrary.Chapter(FSMConstructor._ChapterHash).ChangeState(typeof(MainMenuState));
        }
    }

    private IEnumerator HandleSplash(Image splash)
    {
        float fWaitTime = 1.0f;
        float fFadeTime = 3.0f;

        splash.DOFade(0.0f, 0.0f);
        splash.DOFade(1.0f, fFadeTime);
        yield return new WaitForSeconds(fFadeTime + fWaitTime);
        splash.DOFade(0.0f, fFadeTime).OnComplete(FadeNextImage);
        yield return new WaitForSeconds(fFadeTime + fWaitTime);
    }
    
}
