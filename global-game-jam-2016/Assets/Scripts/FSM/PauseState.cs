﻿using UnityEngine;
using System.Collections;
using FSMLibraryNS;
using StorybookEventsNS;
using System;
using InputEvents;

public class PauseState : BaseState, IReader
{
    private Transform _PauseMenu;

    private string[] _PlayerJoystickIDs;

    public void ReadPage(IPage page)
    {
        if (page is KeyInputEvent)
        {
            KeyInputEvent e = page as KeyInputEvent;
            if (e.BindKey.ToString().Contains("triangle"))
            {
                // change to the main menu
                FSMLibrary.Chapter(FSMConstructor._ChapterHash).ChangeState(typeof(MainMenuState));
            }
            else if (e.BindKey.ToString().Contains("circle"))
            {
                // pop state
                FSMLibrary.Chapter(FSMConstructor._ChapterHash).PopState();
            }
        }
    }

    public override void OnCreate()
    {
        base.OnCreate();
        _PauseMenu = Canvas.FindChild("PauseMenu");
    }

    public override void OnEnter()
    {
        base.OnEnter();
        _PauseMenu.gameObject.SetActive(true);

        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        foreach (string joyID in _PlayerJoystickIDs)
        {
            input.RegisterKey(joyID + "triangle", JoystickAssigner.JoystickKeycode(joyID, 4), BroadcastKeyState.RELEASED);
            input.RegisterKey(joyID + "circle", JoystickAssigner.JoystickKeycode(joyID, 3), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + joyID + "triangle").Subscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + joyID + "circle").Subscribe<KeyInputEvent>(this);
        }

    }

    public override void OnExit()
    {
        base.OnExit();
        _PauseMenu.gameObject.SetActive(false);

        InputBehavior input = GameObject.FindObjectOfType<InputBehavior>();
        foreach (string joyID in _PlayerJoystickIDs)
        {
            input.UnregisterKey(joyID + "triangle", JoystickAssigner.JoystickKeycode(joyID, 4), BroadcastKeyState.RELEASED);
            input.UnregisterKey(joyID + "circle", JoystickAssigner.JoystickKeycode(joyID, 3), BroadcastKeyState.RELEASED);
            StoryBook.Chapter("Input_" + joyID + "triangle").Unsubscribe<KeyInputEvent>(this);
            StoryBook.Chapter("Input_" + joyID + "circle").Unsubscribe<KeyInputEvent>(this);
        }
    }

    public override void RetrievePassthrough(params object[] _PassthroughObjects)
    {
        base.RetrievePassthrough(_PassthroughObjects);
        _PlayerJoystickIDs = _PassthroughObjects as string[];
    }
}
