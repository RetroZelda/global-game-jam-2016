﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RockSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject RockPrefab;

    [SerializeField]
    private BoxCollider _SpawnBox;

    [SerializeField]
    private BoxCollider _SpawnMarkerConstraints;

    [SerializeField]
    private float SpawnStrength = 100.0f;

    [SerializeField]
    private GameObject SpawnMarker;
    private Image SpawnMarkerFGImage;
    private Image SpawnMarkerBGImage;

    [SerializeField]
    private float MarkerSpeed = 10.0f;


	[SerializeField]
	private float SpawnCoolDown = 1.0f;
    private float SpawnTimer = 0.0f;

	[SerializeField]
	private float IgnoreCollisionDistance = 10.0f;

	private CRandomZPlacement[] _Blocks;

    private Color _SpawnMarkerColor = Color.white;
    public Color SpawnMarkerColor
    {
        get { return _SpawnMarkerColor; }
        set
        {
            _SpawnMarkerColor = value;
            SpawnMarkerFGImage.color = _SpawnMarkerColor;
            SpawnMarkerBGImage.color = _SpawnMarkerColor;
        }
    }

    public void Awake()
    {
        SpawnMarkerFGImage = SpawnMarker.transform.FindChild("ShittyArrowFG").GetComponent<Image>();
        SpawnMarkerBGImage = SpawnMarker.transform.FindChild("ShittyArrowBG").GetComponent<Image>();

		_Blocks = GameObject.FindObjectsOfType<CRandomZPlacement>(); 
    }

	public void Update()
	{
		SpawnTimer += Time.deltaTime;
        SpawnMarkerFGImage.fillAmount = Mathf.Clamp(SpawnTimer / SpawnCoolDown, 0.0f, 1.0f);
    }

    public void MoveMarker(Vector3 v3Dir)
    {
        SpawnMarker.transform.Translate(v3Dir * MarkerSpeed, Space.Self);
        Vector3 v3PosVerify = SpawnMarker.transform.localPosition;
        Vector3 v3ScaledSize = _SpawnMarkerConstraints.size / 2.0f;
        v3PosVerify.x = Mathf.Clamp(v3PosVerify.x, -v3ScaledSize.x, v3ScaledSize.x);
        v3PosVerify.y = Mathf.Clamp(v3PosVerify.y, -v3ScaledSize.y, v3ScaledSize.y);
        v3PosVerify.z = Mathf.Clamp(v3PosVerify.z, -v3ScaledSize.z, v3ScaledSize.z);
        SpawnMarker.transform.localPosition = v3PosVerify;
    }

    public void CreateRockAtMarker()
    {
        Vector3 rockPos = _SpawnBox.transform.position;
        rockPos.z = SpawnMarker.transform.position.z;
        rockPos.y += (_SpawnBox.size.y / 2.0f);
        SpawnRock(rockPos, -Vector3.right * SpawnStrength);
    }

    public void CreateRockRandom()
    {
        // create a random pos for the rocks
        Vector3 rockPos = _SpawnBox.transform.position;
        Vector3 v3ScaledSize = _SpawnBox.size / 2.0f;
        rockPos.x += Random.Range(-v3ScaledSize.x, v3ScaledSize.x);
        rockPos.y += _SpawnBox.size.y;// Random.Range(-v3ScaledSize.y, v3ScaledSize.y);
        rockPos.z += Random.Range(-v3ScaledSize.z, v3ScaledSize.z);
        SpawnRock(rockPos, -Vector3.right * SpawnStrength);


    }

	int seed = 0;
    private void SpawnRock(Vector3 pos, Vector3 force)
    {
		
		Random.seed = seed++;

		if(SpawnTimer < SpawnCoolDown)
			return;

		SpawnTimer = 0.0f;

		float LorR = Random.Range(-100.0f,100.0f);


		force += new Vector3(0.0f,0.0f,LorR);




        GameObject newRock = GameObject.Instantiate(RockPrefab, pos, Quaternion.identity) as GameObject;

		foreach(CRandomZPlacement rzp in _Blocks)
		{
			if(Vector3.Distance( newRock.transform.position, rzp.transform.position) < 5.0f)
			{
				Physics.IgnoreCollision(newRock.GetComponent<Collider>(), rzp.GetComponent<Collider>());
				Physics.IgnoreCollision(newRock.GetComponent<Collider>(), rzp.transform.GetChild(0).GetComponent<Collider>());
			}
		}

        Rigidbody rigidRock = newRock.GetComponent<Rigidbody>();
        rigidRock.AddForce(force, ForceMode.Impulse); // potentially velocity change
    }



}
