﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GodManager : MonoBehaviour
{
    private HumanInput[] _Humans;
    private GodInput _God; 
    private GodInput God
    {
        get
        {
            if(_God == null)
                _God = FindObjectOfType<GodInput>();
            return _God;
        }
    }
		
	private RockSpawner _RockSpawner;

    MaterialHolder MatHold;


    // Use this for initialization
    void Start ()
    {
        MatHold = GameObject.FindObjectOfType<MaterialHolder>();
        _RockSpawner = GameObject.FindObjectOfType<RockSpawner>();
        ForceRefresh();

    }

    public void ForceRefresh()
    {
        _Humans = FindObjectsOfType<HumanInput>();
        _God = FindObjectOfType<GodInput>();
    }

    public void StopGod()
    {
        if(God.FocusedJoystick != null)
        {
            // find the player that is controlling us
            foreach(HumanInput human in _Humans)
            {
                if(human.FocusedJoystick == God.FocusedJoystick)
                {
                    God.enabled = false;
                    God.FocusedJoystick = null;
                    human.enabled = true;
                    return;
                }
            }
            Debug.LogWarning("There isnt a player who is controlling God.  Investigate this!");
        }
    }

    public void SetGod(HumanInput human)
    {

        // disable the god
        StopGod();
        God.enabled = false;

        // pull the joystick from the player and give it to the god
        God.FocusedJoystick = human.FocusedJoystick;

        // disable the human
        human.enabled = false;

        // enable the god
        God.enabled = true;

        _RockSpawner.SpawnMarkerColor = MatHold.GetColor(human.Player.ColorIndex);
    }
}
