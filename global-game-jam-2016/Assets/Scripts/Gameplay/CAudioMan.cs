﻿using UnityEngine;
using System.Collections;

public class CAudioMan : MonoBehaviour {

	[SerializeField]
	AudioClip m_MainMenu;
	[SerializeField]
	AudioClip m_Gameplay;
	[SerializeField]
	AudioClip m_Win;


	AudioSource m_Source;

	// Use this for initialization
	void Start () {
	
		m_Source = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayMainMenu()
	{

		m_Source.clip = m_MainMenu;

		m_Source.Play();

	}

	public void PlayGameState()
	{

		m_Source.clip = m_Gameplay;

		m_Source.Play();
	}

	public void PlayWinState()
	{

		m_Source.clip = m_Win;

		m_Source.Play();
		m_Source.Stop();
	}
}
