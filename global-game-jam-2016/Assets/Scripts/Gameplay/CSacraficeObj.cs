﻿using UnityEngine;
using System.Collections;
using StorybookEventsNS;

public class SacrificeCollisionEvent : IPage
{
    public HumanInput Human { get; private set; }
    public SacrificeCollisionEvent(HumanInput human)
    {
        Human = human;
    }
}

public class CSacraficeObj : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter(Collider collision){
        
		HumanInput human = collision.gameObject.GetComponent<HumanInput>();
		CPlayer player = collision.gameObject.GetComponent<CPlayer>();
        if (human == null)
		{
			return;
		}
		player.RigidBod.velocity = new Vector3(0,0,0);
		player.IKilledMyself();

        // throw the event that we have a collisison
        StoryBook.Chapter(GameState.GAME_EVENT_CHAPTER_HASH).Set(new SacrificeCollisionEvent(human));

    }
}
