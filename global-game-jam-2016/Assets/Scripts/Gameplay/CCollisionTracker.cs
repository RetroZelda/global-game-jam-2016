﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CCollisionTracker : MonoBehaviour {

	public List<GameObject> m_CollidingObjs;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
	
		m_CollidingObjs.Clear();
	}

	void OnTriggerStay(Collider collision)
	{
		m_CollidingObjs.Add(collision.gameObject);
	}

	void WipeList()
	{

	}
}
