﻿using UnityEngine;
using System.Collections;
using StorybookEventsNS;


public class CScreenPush : MonoBehaviour {

	[SerializeField]
	CCameraMove cammo = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerStay(Collider collision){

		CPlayer human = collision.gameObject.GetComponent<CPlayer>();

		if(human == null)
		{
			return;
		}

		cammo.SetSpeed(7.0f);
	}

}
