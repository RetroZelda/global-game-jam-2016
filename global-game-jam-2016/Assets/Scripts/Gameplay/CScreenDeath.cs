﻿using UnityEngine;
using System.Collections;
using StorybookEventsNS;

public class ScreenDeathCollisionEvent : IPage
{
    public HumanInput Human { get; private set; }
    public ScreenDeathCollisionEvent(HumanInput human)
    {
        Human = human;
    }
}
public class CScreenDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter(Collider collision){

		HumanInput human = collision.gameObject.GetComponent<HumanInput>();

		if(human == null)
		{
			return;
		}

        StoryBook.Chapter(GameState.GAME_EVENT_CHAPTER_HASH).Set(new ScreenDeathCollisionEvent(human));
	}

}
