﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class CSpawnPositions : MonoBehaviour
{
	private BoxCollider _BoxCol;

	// Use this for initialization
	void Start ()
    {
        _BoxCol = gameObject.GetComponentInChildren<BoxCollider>();
	}
	

	public Vector3[] SpawnPositions(int PlayerCount)
	{
		Vector3 End = transform.position;
        Vector3 Start = transform.position;

        End -= transform.forward * (_BoxCol.size.z / 2.0f);
        Start += transform.forward * (_BoxCol.size.z / 2.0f);
        
        Vector3[] spawnPos = new Vector3[PlayerCount];
        for(int nSpawnCount = 0; nSpawnCount < PlayerCount; ++nSpawnCount)
        {
            spawnPos[nSpawnCount] = Start + ((End - Start) * ((float)(nSpawnCount + 1) / (float)(PlayerCount + 1)));
        }

		return spawnPos;

	}
}
