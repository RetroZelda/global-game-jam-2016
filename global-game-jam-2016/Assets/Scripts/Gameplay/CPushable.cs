﻿using UnityEngine;
using System.Collections;

public class CPushable : MonoBehaviour {
	public Rigidbody RigidBod;

	public Vector3 m_Force;
	public float m_fForceStrength = 0;

	public AudioClip m_AClip;
	public AudioSource m_ASource;
	public AudioSource m_AFootSource;


	public float m_fSpeed = 1.0f;
	public float m_fMAXSPEED = 10.0f;
	public float m_fSLOWDOWNRATE = 0.1f;

	// Use this for initialization
	void Start () {
		RigidBod = GetComponent<Rigidbody>();
		//m_ASource = GetComponent<AudioSource>();

		RigidBod.maxAngularVelocity = 1.0f;


	}
	
	// Update is called once per frame
	void Update () {
		HandleMovement();
	}
		
	public virtual void Pushed(Vector3 m_Location, float strength)
	{
		m_Location.y = transform.position.y;// we dont want to push up ERICK. 
		Vector3 dir = transform.position - m_Location;

		dir.Normalize();

		dir *= strength*100;

		RigidBod.AddForce(dir, ForceMode.Force);

	}
		
	public void PlaySound()
	{
		if(m_ASource == null)
			return;

		m_ASource.clip = m_AClip;
		m_ASource.Play();

	}


	private void HandleMovement()
	{
		CheckVel();
	}

	protected void CheckVel()
	{
		float currentspeed = RigidBod.velocity.magnitude;
		if(currentspeed > m_fMAXSPEED) //Slow down this shiet nawimean?
		{
			RigidBod.velocity = RigidBod.velocity.normalized * (currentspeed - m_fSLOWDOWNRATE * Time.deltaTime);
			currentspeed = RigidBod.velocity.magnitude;


			if(currentspeed <= m_fMAXSPEED) 
				RigidBod.velocity = RigidBod.velocity.normalized * (m_fMAXSPEED);
				
		}

	}

	void Move(Vector3 Dir)
	{
		RigidBod.AddForce(Dir * m_fSpeed, ForceMode.Acceleration);
	}

}
