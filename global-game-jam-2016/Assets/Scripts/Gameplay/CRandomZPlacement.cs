﻿using UnityEngine;
using System.Collections;

public class CRandomZPlacement : MonoBehaviour {

	[SerializeField]
	float m_MinZ;
	
	[SerializeField]
	float m_MaxZ;

	// Use this for initialization
	void Start () {
		
		RANDOMDISSHIET();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RANDOMDISSHIET()
	{
		Vector3 unityisdumb = transform.position;
		unityisdumb.z = Random.Range(m_MinZ, m_MaxZ);
		transform.position = unityisdumb;
	}
}
