﻿using UnityEngine;
using System.Collections;

public class CCameraMove : MonoBehaviour {

	public Vector3 m_vStart;
	public Vector3 m_vEnd;


	public float m_fSETSPEED = 1.0f;

	private float m_fSpeed = 1.0f;

	public BoxCollider Conts;

	public float BoxStart = 40.0f;
	public float BoxEnd = 20.0f;

	// Use this for initialization
	void Start ()
    {
        Reset();
	}
	
	// Update is called once per frame
	void Update () {

		if(m_fSpeed == 0.0f)
			return;
		DoMath();
	}

    public void Reset()
    {
        transform.position = m_vStart;

		Vector3 csharpisgay = Conts.size;
		csharpisgay.z = BoxStart;
		Conts.size = csharpisgay;

        Pause();
    }

	public void Play()
	{
		m_fSpeed = m_fSETSPEED;
	}

	public void Pause()
	{

		m_fSpeed = 0.0f;
	}


	public void SetSpeed(float OneTickSpeed)
	{
		m_fSpeed = OneTickSpeed;

	}

	public void DoMath()
	{
		transform.position = Vector3.MoveTowards(transform.position, m_vEnd, m_fSpeed * Time.deltaTime);
		Vector3 csharpisgay = Conts.size;
		csharpisgay.z = Mathf.MoveTowards(csharpisgay.z, BoxEnd, m_fSpeed *Time.deltaTime);
		Conts.size = csharpisgay;
		m_fSpeed = m_fSETSPEED;
	}


}
