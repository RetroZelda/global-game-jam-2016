﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CGod : MonoBehaviour {

	[SerializeField]
	List<CPushable> m_Pushables;

	public float m_fSwolness = 1.0f;

	public float m_fCoolDownTime = 1.0f;
	private float m_fPushCoolDownTimer = 0.0f;


	CCollisionTracker LeftBumber;
	CCollisionTracker RightBumber;
	// Use this for initialization
	void Start () {

		LeftBumber = GameObject.Find("LeftBumber").GetComponent<CCollisionTracker>();
		RightBumber = GameObject.Find("RightBumber").GetComponent<CCollisionTracker>();
	
		for(int i = 0; i< GameObject.FindObjectsOfType<CPushable>().Length; ++i)
		{
			m_Pushables.Add(GameObject.FindObjectsOfType<CPushable>()[i]);
		}
		//GETPLAYERS5
	}
	
	// Update is called once per frame
	void Update () {

		if(m_fPushCoolDownTimer >= m_fCoolDownTime)
		CheckPush ();


		m_fPushCoolDownTimer += Time.deltaTime;
	}


	public void PushRightWall()
	{
		for(int i = 0; i<RightBumber.m_CollidingObjs.Count; ++i)
		{
			CPushable cpush = RightBumber.m_CollidingObjs[i].GetComponent<CPushable>();
			if(cpush ==null)
				continue;

			Vector3 offset = cpush.gameObject.transform.position;

			offset.x += .5f;
			offset.z -= 1.0f;

			cpush.Pushed(offset, m_fSwolness);
			CPlayer Player = cpush.GetComponent<CPlayer>();
			if(Player!=null)
			{
				Player.StartBumberInputCD();
			}
		}

	}

	public void PushLeftWall()
	{
		for(int i = 0; i<LeftBumber.m_CollidingObjs.Count; ++i)
		{
			CPushable cpush = LeftBumber.m_CollidingObjs[i].GetComponent<CPushable>();
			if(cpush ==null)
				continue;

			Vector3 offset = cpush.gameObject.transform.position;

			offset.x += .5f;
			offset.z += 1.0f;

			cpush.Pushed(offset, m_fSwolness);
			CPlayer Player = cpush.GetComponent<CPlayer>();
			if(Player!=null)
			{
				Player.StartBumberInputCD();
			}
		}
	}


	void CheckPush()
	{

		if (Input.GetMouseButtonDown (1)) 
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit HitInfo = new RaycastHit();

			if(Physics.Raycast(ray, out HitInfo))
			{
				GodPush(HitInfo.point);
				m_fPushCoolDownTimer = 0.0f;
			}


		}


	}

	void GodPush(Vector3 Impact_Point)
	{
		foreach(CPushable go in m_Pushables)
		{

			if(Vector3.Distance(go.transform.position, Impact_Point) < 2.0f)
			{
				go.Pushed(Impact_Point, m_fSwolness);
			}
		}

	}


}
