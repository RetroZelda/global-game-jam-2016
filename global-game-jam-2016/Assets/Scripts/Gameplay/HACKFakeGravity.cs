﻿using UnityEngine;
using System.Collections;

public class HACKFakeGravity : MonoBehaviour {

	Rigidbody RigidBod;

	public float speed = 150.0f;

	// Use this for initialization
	void Start () {

		RigidBod = gameObject.GetComponent<Rigidbody>();

	
	}
	
	// Update is called once per frame
	void Update () {
	
		RigidBod.AddForce(new Vector3(-2.5f,-2.5f,0.0f) * (speed * Time.deltaTime), ForceMode.Acceleration);

	}
}
