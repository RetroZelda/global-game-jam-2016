﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CPlayer : CPushable {
	//Rigidbody RigidBod;
	//
	//Vector3 m_Force;
	//float m_fForceStrength = 0;
	[SerializeField]
	AudioClip m_Clip1;

	[SerializeField]
	AudioClip m_Death;	

	[SerializeField]
	AudioClip m_Death2;	

	[SerializeField]
	AudioClip m_Death3;	

	[SerializeField]
	AudioClip m_Grunt;

	[SerializeField]
	AudioClip m_Grunt2;

	[SerializeField]
	AudioClip m_Grunt3;

	[SerializeField]
	AudioClip m_Grunt4;

	[SerializeField]
	AudioClip m_PlayerHit;

	[SerializeField]
	AudioClip m_FootStep;

	List<CPushable> m_Pushables = new List<CPushable>();

	Transform m_AnimationTransform;

	Animator m_Animator;

	public int m_Points = 0;

	public float m_fDashSpeed = 10.0f;

	float m_fDashCoolDown = 0.0f;
	float m_fDashCoolDownTime = 2.0f;


    public float m_INPUTCD = 0.25f;
	float m_fInputCoolDown = 2.0f;



	private Camera CutSceneCam = null;
    private PlayerUI _UI;
    
    private SkinnedMeshRenderer m_DASKMR;
    private MaterialHolder m_MatHold;
    private int _ColorIndex = -1;
    public int ColorIndex
    {
        get { return _ColorIndex; }
        private set { _ColorIndex = value; }
    }

    public bool UIEnable
    {
        get
        {
            return _UI.UI.gameObject.activeSelf;
        }
        set
        {
            _UI.UI.gameObject.SetActive(value);
        }
    }

    // Use this for initialization
    void Start () {
        
        //m_ASource = GetComponent<AudioSource>();
        m_AFootSource.clip = m_FootStep;
		m_AnimationTransform = gameObject.transform.GetChild(0);//LOL

		m_Animator = m_AnimationTransform.GetComponent<Animator>();

		RigidBod = GetComponent<Rigidbody>();

		RigidBod.maxAngularVelocity = 1.0f;

		for(int i = 0; i< GameObject.FindObjectsOfType<CPlayer>().Length; ++i)
		{
			m_Pushables.Add(GameObject.FindObjectsOfType<CPlayer>()[i]);
		}

        ResetColor();
    }

    public void Awake()
    {
        CutSceneCam = GameObject.Find("CutSceneCamera").GetComponent<Camera>();
        m_MatHold = GameObject.FindObjectOfType<MaterialHolder>();
        _UI = GetComponent<PlayerUI>();
    }

    public void AquireAColor(bool bForce = false)
    {
        if (ColorIndex == -1 || bForce)
        {
            SetColor(m_MatHold.NextColorIndex());
        }
    }

    public void ResetColor()
    {
        if (ColorIndex == -1)
        {
            SetColor(m_MatHold.NextColorIndex());
        }
        else
        {
            SetColor(ColorIndex);
        }
    }

    public void SetColor(int nColorIndex)
    {
        ColorIndex = nColorIndex;
        m_DASKMR = gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>();
        m_DASKMR.material = m_MatHold.GetMaterial(ColorIndex);
        _UI.PlayerColor = m_MatHold.GetColor(ColorIndex);
    }

    public void SetColor(Color col)
    {
        ColorIndex = m_MatHold.GetColorIndex(col);
        m_DASKMR = gameObject.transform.GetComponentInChildren<SkinnedMeshRenderer>();
        m_DASKMR.material = m_MatHold.GetMaterial(ColorIndex);
        _UI.PlayerColor = m_MatHold.GetColor(ColorIndex);
    }

    void OnCollisionEnter(Collision collision)
	{
		if( collision.gameObject.GetComponent<CPushable>() == false )
			return;

		m_fInputCoolDown = 0.0f;
		m_Animator.SetTrigger("Front");
		CPlayer playa = collision.gameObject.GetComponent<CPlayer>();
		if (playa == true)
        {
			collision.gameObject.GetComponent<CPlayer>().Pushed(transform.position, playa.RigidBod.velocity.magnitude*2.0f);
            ReactToPlayer();

        }
        else if (collision.gameObject.tag == "Rock")
        {
            ReactToBoulder();
        }
    }

	public void StartBumberInputCD()
	{
		m_fInputCoolDown = -.5f; // make them wait a second


	}

	void ReactToPlayer()
	{
		m_ASource.clip = m_PlayerHit;	
		m_ASource.Play();
	}

	void ReactToBoulder()
	{
		PlayRandomGrunt();
	}

    public void PlayDeath()
    {
        int RandGrunt = Random.Range(0, 2);
        Debug.Log("Random grunt" + RandGrunt.ToString());
        if (RandGrunt == 0)
        {
            m_ASource.clip = m_Death;

        }

        if (RandGrunt == 1)
        {
            m_ASource.clip = m_Death2;
        }

        if (RandGrunt == 2)
        {
            m_ASource.clip = m_Death3;
        }

        m_ASource.Play();
    }


    void PlayRandomGrunt()
	{
		int RandGrunt = Random.Range(0, 3);
        Debug.Log("PLAY GRUNT");

        if(m_ASource.isPlaying)
            return;
        Debug.Log("PLAY GRUNT2");
        if (RandGrunt == 0)
		{
			m_ASource.clip = m_Grunt;
		}

		if (RandGrunt == 1)
		{
			m_ASource.clip = m_Grunt2;
		}

		if (RandGrunt == 2)
		{
			m_ASource.clip = m_Grunt3;
		}

		if (RandGrunt == 3)
		{
			m_ASource.clip = m_Grunt4;
		}

		m_ASource.Play();
	}

	// Update is called once per frame
	void Update () {
		base.CheckVel();

		m_fDashCoolDown += Time.deltaTime;
		m_fInputCoolDown += Time.deltaTime;

        _UI.SetDashCooldownLamba(Mathf.Clamp(m_fDashCoolDown / m_fDashCoolDownTime, 0.0f, 1.0f));

        //m_ASource.clip = m_AClip;
        //m_ASource.Play();
    }
		
	public override void Pushed(Vector3 m_Location, float strength)
	{
		m_Location.y = transform.position.y;// we dont want to push up ERICK. 
		Vector3 dir = transform.position - m_Location;
	
	
		dir.Normalize();
	
		dir *= strength;
	
		RigidBod.AddForce(dir, ForceMode.VelocityChange);
		m_Animator.SetTrigger("Front");
		base.PlaySound();
	
	}

    public void IKilledMyself()
    {
        m_Points += 1;
		m_Animator.SetTrigger("Win");


		TurnOnCut();
    }

	public void TurnOnCut()
	{
		CutSceneCam.depth = 1.0f;
		transform.position = GameObject.Find("USE").transform.position;
		m_AnimationTransform.rotation = GameObject.Find("USE").transform.rotation;

	}

	public void TurnOffCut()
	{
		CutSceneCam.depth = -2.0f;
	}

    public void WonAsGod()
    {
        m_Points += 1;
    }


    public void Dash(Vector3 Dir)
	{
		if(m_fDashCoolDown < m_fDashCoolDownTime || Dir.magnitude < 0.5f)
			return;

		RigidBod.AddForce(Dir * m_fDashSpeed, ForceMode.Impulse);

		m_Animator.SetTrigger("Dash");

		m_fDashCoolDown = 0.0f;

		m_ASource.clip = m_Clip1;

		m_ASource.Play();
	}

	public void PushPlayers()
	{
		foreach(CPlayer player in m_Pushables)
		{
			if(Vector3.Distance( player.transform.position, transform.position) < 2.0f)
			{
				player.Pushed(transform.position, 10.0f);

			}

		}
	}

	public void Move(Vector3 Dir)
	{
		if(m_fInputCoolDown < m_INPUTCD)
			return;

		RigidBod.AddForce(Dir * m_fSpeed, ForceMode.Acceleration);

		if(Dir.magnitude < .5f)
			return;
		
		m_AnimationTransform.localRotation = Quaternion.LookRotation(Dir);

		m_Animator.SetFloat("Vel", 0.75f);
        if (!m_AFootSource.isPlaying)
            m_AFootSource.Play();
	}

}
