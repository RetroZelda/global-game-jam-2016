﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaterialHolder : MonoBehaviour {

	[SerializeField]
	private Material _TargetMaterial;

    [SerializeField]
    public List<Color> _Colors;

    private int IndexGiver = -1;

	public int NextColorIndex()
	{
        IndexGiver++;
        if (IndexGiver >= _Colors.Count)
            IndexGiver = 0;

        return IndexGiver;
	}

    public Color GetColor(int nIndex)
    {
        return _Colors[nIndex];
    }

    public int GetColorIndex(Color col)
    {
        int nIndex =  _Colors.FindIndex(c => c.Equals(col));
        if(nIndex == -1)
        {
            // not found, so add the color
            nIndex = _Colors.Count;
            _Colors.Add(col);
        }
        return nIndex;
    }

    public Material GetMaterial()
    {
        Material newMat = Object.Instantiate<Material>(_TargetMaterial);
        newMat.color = _Colors[NextColorIndex()];
        return newMat;
    }

    public Material GetMaterial(int nColorIndex)
    {
        Material newMat = Object.Instantiate<Material>(_TargetMaterial);
        newMat.color = _Colors[nColorIndex];
        return newMat;
    }


}
