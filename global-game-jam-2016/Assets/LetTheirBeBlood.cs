﻿using UnityEngine;
using System.Collections;

public class LetTheirBeBlood : StateMachineBehaviour {

	[SerializeField]
	float SpawnBloodTime = 3.17f;

	private float TIMER = 0.0f;

	GameObject BLOOD;

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TIMER = 0.0f;
		BLOOD = animator.transform.GetComponentInChildren<CParticleFinder>().gameObject;
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		TIMER += Time.deltaTime;

		if(SpawnBloodTime <= TIMER && BLOOD != null)
		{
			BLOOD.GetComponent<ParticleSystem>().Play();
			TIMER = -10000000000000.0f;
		}

	}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
		BLOOD.GetComponent<ParticleSystem>().Stop();
	}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
